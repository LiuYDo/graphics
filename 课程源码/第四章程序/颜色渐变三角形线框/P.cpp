// P.cpp: implementation of the CP class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Test.h"
#include "P.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CP::CP()
{

}

CP::~CP()
{

}
CP::CP(double x,double y)
{
	this->x=x;
	this->y=y;
}
CP::CP(double x,double y,CRGB c)
{
	this->x=x;
	this->y=y;
	this->c=c;
}
