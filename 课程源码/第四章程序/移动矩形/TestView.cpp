// TestView.cpp : implementation of the CTestView class
//

#include "stdafx.h"
#include "Test.h"

#include "TestDoc.h"
#include "TestView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestView

IMPLEMENT_DYNCREATE(CTestView, CView)

BEGIN_MESSAGE_MAP(CTestView, CView)
	//{{AFX_MSG_MAP(CTestView)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestView construction/destruction

CTestView::CTestView()
{
	// TODO: add construction code here
	p=CPoint(400,300);
	bMove=false;
	clr=RGB(0,0,0,);
	nPenStyle=0;
}

CTestView::~CTestView()
{
}

BOOL CTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CTestView drawing

void CTestView::OnDraw(CDC* pDC)
{
	CTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	GetClientRect(rect);
	CDC memDC;
	memDC.CreateCompatibleDC(pDC);
	CBitmap bmp;
	bmp.CreateCompatibleBitmap(pDC,rect.Width(),rect.Height());
	memDC.SelectObject(bmp);
	memDC.FillSolidRect(rect,pDC->GetBkColor());
	DrawRectangle(&memDC);
	pDC->BitBlt(0, 0 , rect.Width() ,rect.Height() ,&memDC, 0, 0, SRCCOPY);
}
	
/////////////////////////////////////////////////////////////////////////////
// CTestView printing

BOOL CTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CTestView diagnostics

#ifdef _DEBUG
void CTestView::AssertValid() const
{
	CView::AssertValid();
}

void CTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestDoc* CTestView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestDoc)));
	return (CTestDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTestView message handlers
void CTestView::DrawRectangle(CDC* pDC) 
{
	CPen NewPen,*pOldPen;
	NewPen.CreatePen(nPenStyle,1,clr);
	pOldPen=pDC->SelectObject(&NewPen);
	nHalfWidth=200,nHalfHeight=100;
	pDC->Rectangle(p.x - nHalfWidth , p.y - nHalfHeight ,p.x + nHalfWidth ,p.y + nHalfHeight);
	pDC->SelectObject(pOldPen);
}
void CTestView::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	if(bMove)
		p=point;
	if(point.x < p.x + nHalfWidth  && point.x > p.x - nHalfWidth  &&  point.y < p.y + nHalfHeight  && point.y > p.y - nHalfHeight ) 
		SetCursor(LoadCursor(NULL,IDC_SIZEALL));
	BoundaryCollisionDetection();
	Invalidate(false);
	CView::OnMouseMove(nFlags, point);
}

void CTestView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	if(point.x < p.x + nHalfWidth  && point.x > p.x - nHalfWidth  &&  point.y < p.y + nHalfHeight  && point.y > p.y - nHalfHeight ) 
	{
		SetCursor(LoadCursor(NULL,IDC_SIZEALL));
		p=point;
		bMove=true;
		clr=RGB(128,0,0);
		nPenStyle=1;
	}
	Invalidate(false);	
	CView::OnLButtonDown(nFlags, point);
}

void CTestView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	bMove=false;
	clr=RGB(0,0,0);
	nPenStyle=0;
	Invalidate(false);
	CView::OnLButtonUp(nFlags, point);
}
void CTestView::BoundaryCollisionDetection() 
{
	if(p.x + nHalfWidth >= rect.right)
		p.x = rect.right - nHalfWidth;
	if(p.x - nHalfWidth <= rect.left)
		p.x = rect.left + nHalfWidth;
	if(p.y + nHalfHeight >= rect.bottom)
		p.y = rect.bottom - nHalfHeight;
	if(p.y - nHalfHeight <= rect.top)
		p.y = rect.top + nHalfHeight;
}
