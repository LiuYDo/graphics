// p3_9View.cpp : implementation of the CP3_9View class
//

#include "stdafx.h"
#include "p3_9.h"
#include "Circle.h"
#include "p3_9Doc.h"
#include "p3_9View.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CP3_9View

IMPLEMENT_DYNCREATE(CP3_9View, CView)

BEGIN_MESSAGE_MAP(CP3_9View, CView)
	//{{AFX_MSG_MAP(CP3_9View)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CP3_9View construction/destruction

CP3_9View::CP3_9View()
{
	// TODO: add construction code here

}

CP3_9View::~CP3_9View()
{
}

BOOL CP3_9View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CP3_9View drawing

void CP3_9View::OnDraw(CDC* pDC)
{
	CP3_9Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here

	CRect rect;
	GetClientRect(&rect);
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(rect.Width(),rect.Height());
	pDC->SetViewportExt(rect.Width(),-rect.Height());
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);
	rect.OffsetRect(-rect.Width()/2,-rect.Height()/2);

	pDC->FillSolidRect(rect,RGB(96,96,96));//���û�ɫ����


	CCircle c;
	c.drawCirclep(pDC,-200,0,100.00);
	c.drawCirclep2(pDC,-200,0,15);
	c.drawCircle(pDC,200,0,100.00);
	c.drawCirclep2(pDC,200,0,15);

}

/////////////////////////////////////////////////////////////////////////////
// CP3_9View printing

BOOL CP3_9View::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CP3_9View::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CP3_9View::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CP3_9View diagnostics

#ifdef _DEBUG
void CP3_9View::AssertValid() const
{
	CView::AssertValid();
}

void CP3_9View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CP3_9Doc* CP3_9View::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CP3_9Doc)));
	return (CP3_9Doc*)m_pDocument;
}
#endif //_DEBUG

