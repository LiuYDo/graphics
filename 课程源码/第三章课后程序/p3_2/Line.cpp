// Line.cpp: implementation of the CLine class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Line.h"
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLine::CLine()
{

}
double Round(double val)
{
    return (val> 0.0) ? floor(val+ 0.5) : ceil(val- 0.5);
}
CLine::~CLine()
{

}
void CLine::MoveTo(CDC *pDC,CP2 p0)
{
	this->p0 = p0;
}
void CLine::MoveTo(CDC *pDC,double x0,double y0)
{
	this->p0 = CP2(x0,y0);
}
void CLine::LineTo(CDC *pDC,CP2 p1)
{
	this->p1 = p1;
	CP2 p;
	COLORREF clr=RGB(0,0,0);
	this->k = (p1.y-p0.y)/(p1.x-p0.x);//0<=k<=1;x0<x1
	for(p=p0;p.x<p1.x;p.x++)
	{
		pDC->SetPixelV(Round(p.x),Round(p.y),clr);
		p.y=p.y+k;
	}
}
void CLine::LineTo(CDC *pDC,double x1,double y1)
{
	LineTo(pDC,CP2(x1,y1));
}