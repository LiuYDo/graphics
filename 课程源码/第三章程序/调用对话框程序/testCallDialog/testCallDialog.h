// testCallDialog.h : main header file for the TESTCALLDIALOG application
//

#if !defined(AFX_TESTCALLDIALOG_H__825F3E36_C169_4FCF_9EA7_74992E5FCD7A__INCLUDED_)
#define AFX_TESTCALLDIALOG_H__825F3E36_C169_4FCF_9EA7_74992E5FCD7A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CTestCallDialogApp:
// See testCallDialog.cpp for the implementation of this class
//

class CTestCallDialogApp : public CWinApp
{
public:
	CTestCallDialogApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTestCallDialogApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CTestCallDialogApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTCALLDIALOG_H__825F3E36_C169_4FCF_9EA7_74992E5FCD7A__INCLUDED_)
