// Task.h : main header file for the TASK application
//

#if !defined(AFX_TASK_H__DFF8EC40_4868_4A01_9369_71E871C4C2EC__INCLUDED_)
#define AFX_TASK_H__DFF8EC40_4868_4A01_9369_71E871C4C2EC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CTaskApp:
// See Task.cpp for the implementation of this class
//

class CTaskApp : public CWinApp
{
public:
	CTaskApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTaskApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CTaskApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TASK_H__DFF8EC40_4868_4A01_9369_71E871C4C2EC__INCLUDED_)
