// TaskView.cpp : implementation of the CTaskView class
//

#include "stdafx.h"
#include "Task.h"

#include "TaskDoc.h"
#include "TaskView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTaskView

IMPLEMENT_DYNCREATE(CTaskView, CView)

BEGIN_MESSAGE_MAP(CTaskView, CView)
	//{{AFX_MSG_MAP(CTaskView)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTaskView construction/destruction

CTaskView::CTaskView()
{
	// TODO: add construction code here
	p0=p1=CPoint(0,0);
}

CTaskView::~CTaskView()
{
}

BOOL CTaskView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CTaskView drawing

void CTaskView::OnDraw(CDC* pDC)
{
	CTaskDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	pDC->MoveTo(p0);
	pDC->LineTo(p1);
}

/////////////////////////////////////////////////////////////////////////////
// CTaskView printing

BOOL CTaskView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CTaskView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CTaskView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CTaskView diagnostics

#ifdef _DEBUG
void CTaskView::AssertValid() const
{
	CView::AssertValid();
}

void CTaskView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTaskDoc* CTaskView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTaskDoc)));
	return (CTaskDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTaskView message handlers

void CTaskView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	p0=point;

	CView::OnLButtonDown(nFlags, point);  //调用默认的鼠标单击处理函数，执行默认的例如刷新、填充背景色等等一系列动作。
}

void CTaskView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	p1=point;
	Invalidate(false);  // 不擦除背景，直接画
 // Invalidate(true);  // 擦除背景再画
	CView::OnLButtonUp(nFlags, point);//调用默认的鼠标单击处理函数，执行默认的例如刷新、填充背景色等等一系列动作。
}
