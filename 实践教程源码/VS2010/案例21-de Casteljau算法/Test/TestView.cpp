
// TestView.cpp : CTestView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "Test.h"
#endif

#include "TestDoc.h"
#include "TestView.h"
#include "math.h"//包含数学头文件
#define  PI 3.1415926//PI的宏定义
#define Round(d) int(floor(d+0.5))//四舍五入宏定义
#define N_MAX_POINT 21//控制多边形的最大顶点数
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTestView

IMPLEMENT_DYNCREATE(CTestView, CView)

BEGIN_MESSAGE_MAP(CTestView, CView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_COMMAND(IDM_DRAWPIC, &CTestView::OnDrawpic)
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
END_MESSAGE_MAP()

// CTestView 构造/析构

CTestView::CTestView()
{
	// TODO: 在此处添加构造代码
	P=new CPoint[N_MAX_POINT];
	bFlag=FALSE;
	CtrlPointNum=0;

}

CTestView::~CTestView()
{
	if(P!=NULL)
	{
		delete []P;
		P=NULL;
	}
}

BOOL CTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CView::PreCreateWindow(cs);
}

// CTestView 绘制

void CTestView::OnDraw(CDC* /*pDC*/)
{
	CTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 在此处为本机数据添加绘制代码
}


// CTestView 打印

BOOL CTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加额外的打印前进行的初始化过程
}

void CTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加打印后进行的清理过程
}


// CTestView 诊断

#ifdef _DEBUG
void CTestView::AssertValid() const
{
	CView::AssertValid();
}

void CTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestDoc* CTestView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestDoc)));
	return (CTestDoc*)m_pDocument;
}
#endif //_DEBUG


// CTestView 消息处理程序


void CTestView::OnDrawpic() 
{
	// TODO: Add your command handler code here
	MessageBox(CString("左键绘制控制多边形，右键绘制曲线"),CString("提示"),MB_OK);
	RedrawWindow();
	bFlag=TRUE;
	CtrlPointNum=0;
}
void CTestView::DrawBezier()//绘制Bezier曲线
{
	CDC *pDC=GetDC();
	CPen NewPen,*pOldPen;
	NewPen.CreatePen(PS_SOLID,1,RGB(0,0,255));//控制多边形
	pOldPen=pDC->SelectObject(&NewPen);
	CP2 *pt=new CP2[CtrlPointNum];
	pp=new CP2 *[CtrlPointNum];//设置行指针
	for(int i=0;i<CtrlPointNum;i++)
		pp[i]=new CP2[CtrlPointNum];//分配一个一维数组为一行
	pDC->MoveTo(P[0]);
	for(int k=0;k<=n;k++)
	{
		pt[k].x=P[k].x;
		pt[k].y=P[k].y;
	}
    for(double t=0.0;t<=1.0;t+=0.01)
	{
		deCasteljau(t,pt);
	    pDC->LineTo(Round(pp[0][n].x),Round(pp[0][n].y));	
	}
	NewPen.DeleteObject();
	ReleaseDC(pDC);
	if(pt!=NULL)
	{
		delete []pt;
		pt=NULL;
	}
	for(int j=0;j<CtrlPointNum;j++)
	{
		delete[] pp[j];
		pp[j]=NULL;
	}
	delete[] pp;
	pp=NULL;
}

void CTestView::deCasteljau(double t,CP2 *p)//de Casteljau函数
{
	for(int k=0;k<=n;k++)
		pp[k][0]=p[k];
	for(int r=1;r<=n;r++)//de Casteljau递推公式
	{
		for(int i=0;i<=n-r;i++)
		{ 
			pp[i][r].x=(1-t)*pp[i][r-1].x+t*pp[i+1][r-1].x;
			pp[i][r].y=(1-t)*pp[i][r-1].y+t*pp[i+1][r-1].y;
		} 
    } 
}

void CTestView::DrawCtrlPolygon()//绘制控制多边形
{
	CDC *pDC=GetDC();
	CBrush NewBrush,*pOldBrush;
	pOldBrush=(CBrush*)pDC->SelectStockObject(GRAY_BRUSH);//选择灰色库画刷
	for(int i=0;i<=n;i++)
	{
		if(0==i)
		{
			pDC->MoveTo(P[i]);
			pDC->Ellipse(P[i].x-2,P[i].y-2,P[i].x+2,P[i].y+2);
		}
		else
		{
			pDC->LineTo(P[i]);
			pDC->Ellipse(P[i].x-2,P[i].y-2,P[i].x+2,P[i].y+2);
		}
	}
	pDC->SelectObject(pOldBrush);
	ReleaseDC(pDC);
}
void CTestView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if(bFlag)
	{
		P[CtrlPointNum].x=point.x;
		P[CtrlPointNum].y=point.y;
		if(CtrlPointNum<N_MAX_POINT-1)
			CtrlPointNum++;
		else
			bFlag=FALSE;
		n=CtrlPointNum-1;
		DrawCtrlPolygon();
	}
	CView::OnLButtonDown(nFlags, point);
}


void CTestView::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	bFlag=FALSE;
	if(0!=CtrlPointNum)
		DrawBezier();
	CView::OnRButtonDown(nFlags, point);
}
