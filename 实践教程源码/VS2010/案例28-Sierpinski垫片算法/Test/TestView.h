
// TestView.h : CTestView 类的接口
//

#pragma once
#include "InputDlg.h"//对话框头文件
#include "P2.h"
class CTestView : public CView
{
protected: // 仅从序列化创建
	CTestView();
	DECLARE_DYNCREATE(CTestView)

// 特性
public:
	CTestDoc* GetDocument() const;

// 操作
public:
	void FillTriangle(CP2 p0,CP2 p1,CP2 p2);
	void Gasket(int,CP2,CP2,CP2);//垫片函数
// 重写
public:

	virtual void OnDraw(CDC* pDC);  // 重写以绘制该视图
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 实现
public:
	virtual ~CTestView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	
	CDC *pDC;//设备上下文对象
	int n;//递归深度	
// 生成的消息映射函数
protected:

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDrawpic();
};

#ifndef _DEBUG  // TestView.cpp 中的调试版本
inline CTestDoc* CTestView::GetDocument() const
   { return reinterpret_cast<CTestDoc*>(m_pDocument); }
#endif

