//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Test.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_FORMVIEW                    101
#define IDR_MAINFRAME                   128
#define IDR_TestTYPE                    130
#define IDB_BITMAP1                     311
#define IDI_ICON1                       312
#define IDI_ICON2                       313
#define IDI_ICON3                       314
#define IDB_BITMAP2                     315
#define IDB_TEXTURE                     315
#define IDB_BUMPTEXTURE                 315
#define IDB_CLRTEXTURE                  315
#define IDB_BITMAP3                     316
#define IDB_BUMPTEXTURE2                316
#define IDC_AMBIENT                     1001
#define IDC_DIFFUSE                     1002
#define IDC_SPECULAR                    1003
#define IDC_GOLD                        1004
#define IDC_RUBY                        1005
#define IDC_SILVER                      1006
#define IDC_BERYL                       1007
#define IDC_LEFTTOP                     1012
#define IDC_LEFTDOWN                    1013
#define IDC_RIGHTTOP                    1014
#define IDC_GOLD5                       1015
#define IDC_RIGHTDOWN                   1015
#define ID_BUTTON32771                  32771
#define ID_BUTTON32772                  32772
#define ID_BUTTON32773                  32773
#define ID_DRAWPIC                      32774
#define IDM_DRAWPIC                     32775
#define IDM_PLAY                        32776

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        317
#define _APS_NEXT_COMMAND_VALUE         32777
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
