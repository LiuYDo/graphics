
// TestView.h : CTestView 类的接口
//

#pragma once
#include "P3.h"//包含三维坐标点类
#include "P2.h"//包含二维坐标点类
class CTestView : public CView
{
protected: // 仅从序列化创建
	CTestView();
	DECLARE_DYNCREATE(CTestView)

// 特性
public:
	CTestDoc* GetDocument() const;

// 操作
public:
	void ReadPoint();//读入特征多边形的顶点
	void ObliqueProjection();//斜等测投影
	void LeftMultiMatrix(double M0[][4],CP3 P0[][4]); //左乘矩阵
	void RightMultiMatrix(CP3 P0[][4],double M0[][4]);//右乘矩阵
	void TransposeMatrix(double M0[][4]);//矩阵转置
	void DoubleBuffer(CDC *pDC);//双缓冲绘图
	void DrawObject(CDC *);//绘制Bezier曲面
	void DrawCtrlPolygon(CDC *);//绘制控制多边形
	void SignCtrPoint(CDC *);//标注控制点
public:

	virtual void OnDraw(CDC* pDC);  // 重写以绘制该视图
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 实现
public:
	virtual ~CTestView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CP3 P3[4][4];//三维顶点
	CP2 P2[4][4];//二维顶点
	double MT[4][4];//M的转置矩阵
	bool flag;//是否标注控制点
// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDrawpic();
//	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
//	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
//	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	virtual void OnInitialUpdate();
};

#ifndef _DEBUG  // TestView.cpp 中的调试版本
inline CTestDoc* CTestView::GetDocument() const
   { return reinterpret_cast<CTestDoc*>(m_pDocument); }
#endif

