
// TestView.cpp : CTestView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "Test.h"
#endif

#include "TestDoc.h"
#include "TestView.h"
#include "math.h"//包含数学头文件
#define  PI 3.1415926//PI的宏定义
#define Round(d) int(floor(d+0.5))//四舍五入宏定义
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTestView

IMPLEMENT_DYNCREATE(CTestView, CView)

BEGIN_MESSAGE_MAP(CTestView, CView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_COMMAND(IDM_DRAWPIC, &CTestView::OnDrawpic)
END_MESSAGE_MAP()

// CTestView 构造/析构

CTestView::CTestView()
{
	// TODO: 在此处添加构造代码
	pDC=NULL;
}

CTestView::~CTestView()
{
}

BOOL CTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CView::PreCreateWindow(cs);
}

// CTestView 绘制

void CTestView::OnDraw(CDC* /*pDC*/)
{
	CTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 在此处为本机数据添加绘制代码
}


// CTestView 打印

BOOL CTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加额外的打印前进行的初始化过程
}

void CTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加打印后进行的清理过程
}


// CTestView 诊断

#ifdef _DEBUG
void CTestView::AssertValid() const
{
	CView::AssertValid();
}

void CTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestDoc* CTestView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestDoc)));
	return (CTestDoc*)m_pDocument;
}
#endif //_DEBUG


// CTestView 消息处理程序


void CTestView::Initial(int n)//文法模型
{	
	StackPushPos=0;
	Axiom="F";
	Rule="FF[++F-F-F][-F+F+F]";
	NewRule=Axiom;
	NewRuleTemp.Empty();
	int Length=NewRule.GetLength();
	for(int i=1;i<=n;i++)//从n＝1开始替换，n＝0时，就是"F"
	{		
		int Pos=0;
		for(int j=0;j<Length;j++)//规则替换
		{
			if(Axiom==NewRule[j])
			{
				NewRuleTemp+=Rule;
				Pos=NewRuleTemp.GetLength()-1 ;
			}
			else
			{
				NewRuleTemp+=NewRule[j];
				Pos++;
			}
		}
		NewRule=NewRuleTemp;
		NewRuleTemp.Empty();
		Length=NewRule.GetLength();
	}	
}
	
void CTestView::Grass(double theta,double d)//绘图规则
{
	if(NewRule.IsEmpty ())//字符串空返回
		return ;
	else 
	{
		CStateNode CurrentNode,NextNode;
  		CurrentNode.x=P0.x;
		CurrentNode.y=P0.y;
		CurrentNode.alpha=PI/2;
		int Len=NewRule.GetLength();
		pDC->MoveTo(Round(CurrentNode.x),Round(CurrentNode.y));
		for(int i=0;i<Len;i++)
		{
			switch(NewRule[i])//访问字符串中的某个位置的字符
			{
				case 'F'://取出"F"字符的操作
		 			NextNode.x=CurrentNode.x+d*cos(CurrentNode.alpha);
					NextNode.y=CurrentNode.y+d*sin(CurrentNode.alpha);
					NextNode.alpha=CurrentNode.alpha;
					pDC->LineTo(Round(NextNode.x),Round(NextNode.y));
					CurrentNode=NextNode;
					break ;			
				case '['://取出"["字符的操作
					Stack[StackPushPos]=CurrentNode;
					StackPushPos ++;
					break;
				case ']'://取出"]"字符的操作
					CurrentNode=Stack[StackPushPos-1]; 
					StackPushPos -- ;
					pDC->MoveTo(Round(CurrentNode.x),Round(CurrentNode.y));
					break;
				case '+'://取出"+"字符的操作
					CurrentNode.alpha=CurrentNode.alpha+theta;
					break;
				case '-'://取出"-"字符的操作
					CurrentNode.alpha=CurrentNode.alpha-theta;
					break;
				default:
					break;
			}			
		}
	}
}
 
void CTestView::OnDrawpic() 
{
	// TODO: Add your command handler code here
	CInputDlg dlg;
	int n;
	double d,Theta;
	if(IDOK==dlg.DoModal())
	{
		n=dlg.m_n;
	    Theta=dlg.m_th;
		d=dlg.m_d;
	}
	else
		return;
	RedrawWindow();
	pDC=GetDC();
	CRect rect;
	GetClientRect(&rect);	
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(rect.Width(),rect.Height());
	pDC->SetViewportExt(rect.Width(),-rect.Height());
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);
	rect.OffsetRect(-rect.Width()/2,-rect.Height()/2);	
	P0.x=0,P0.y=-rect.Height()/2.0;//起点坐标
	Theta=Theta*PI/180;
	Initial(n);	
	Grass(Theta,d);
	ReleaseDC(pDC);
}
