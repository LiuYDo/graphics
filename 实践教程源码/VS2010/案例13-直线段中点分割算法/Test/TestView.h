
// TestView.h : CTestView 类的接口
//

#pragma once

#include "P2.h"//包含点类
class CTestView : public CView
{
protected: // 仅从序列化创建
	CTestView();
	DECLARE_DYNCREATE(CTestView)

// 特性
public:
	CTestDoc* GetDocument() const;

// 操作
public:

// 重写
public:
	void DoubleBuffer(CDC *pDC);//双缓冲
	void DrawWindowRect(CDC *);//绘制裁剪窗口
	BOOL Cohen();//裁剪函数
	void EnCode(CP2 &);//编码函数
	CP2  Convert(CPoint point);//坐标系转换
	void Diamond(CDC *pDC);//绘制金刚石的函数
	void MidClip(CP2,CP2);//中点分割函数
	virtual void OnDraw(CDC* pDC);  // 重写以绘制该视图
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 实现
public:
	virtual ~CTestView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CP2 P[2];//直线的起点和终点
	int PtCount;//顶点个数
	BOOL bDrawRect;//是否允许画线
	BOOL bClip;//是否裁剪
	CP2  *V;//动态定义等分点数组
	CP2  Rect[2];//定义裁剪窗口对角坐标
// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDrawpic();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
//	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnClip();
	afx_msg void OnUpdateClip(CCmdUI *pCmdUI);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};

#ifndef _DEBUG  // TestView.cpp 中的调试版本
inline CTestDoc* CTestView::GetDocument() const
   { return reinterpret_cast<CTestDoc*>(m_pDocument); }
#endif

