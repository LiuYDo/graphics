// InputDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "Test.h"
#include "InputDlg.h"
#include "afxdialogex.h"


// CInputDlg 对话框

IMPLEMENT_DYNAMIC(CInputDlg, CDialogEx)

CInputDlg::CInputDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CInputDlg::IDD, pParent)
	, m_m(6)
	, m_theta(60)
{

}

CInputDlg::~CInputDlg()
{
}

void CInputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_m);
	DDV_MinMaxInt(pDX, m_m, 0, 10);
	DDX_Text(pDX, IDC_EDIT3, m_theta);
	DDV_MinMaxInt(pDX, m_theta, 0, 89);
}


BEGIN_MESSAGE_MAP(CInputDlg, CDialogEx)
END_MESSAGE_MAP()


// CInputDlg 消息处理程序
