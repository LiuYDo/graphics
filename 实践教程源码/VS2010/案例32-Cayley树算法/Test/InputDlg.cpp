// InputDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "Test.h"
#include "InputDlg.h"
#include "afxdialogex.h"


// CInputDlg 对话框

IMPLEMENT_DYNAMIC(CInputDlg, CDialogEx)

CInputDlg::CInputDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CInputDlg::IDD, pParent)
	, m_m(13)
	, m_height(200)
	, m_alpha(60)
{

}

CInputDlg::~CInputDlg()
{
}

void CInputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_m);
	DDV_MinMaxInt(pDX, m_m, 0, 50);
	DDX_Text(pDX, IDC_EDIT3, m_height);
	DDV_MinMaxInt(pDX, m_height, 0, 500);
	DDX_Text(pDX, IDC_EDIT4, m_alpha);
	DDV_MinMaxInt(pDX, m_alpha, 0, 89);
}


BEGIN_MESSAGE_MAP(CInputDlg, CDialogEx)
END_MESSAGE_MAP()


// CInputDlg 消息处理程序
