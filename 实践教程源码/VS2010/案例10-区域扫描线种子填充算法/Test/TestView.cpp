
// TestView.cpp : CTestView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "Test.h"
#endif

#include "TestDoc.h"
#include "TestView.h"
#include "math.h"//包含数学头文件
#define  PI 3.1415926//PI的宏定义
#define Round(d) int(floor(d+0.5))//四舍五入宏定义

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTestView

IMPLEMENT_DYNCREATE(CTestView, CView)

BEGIN_MESSAGE_MAP(CTestView, CView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_COMMAND(IDM_DRAWPIC, &CTestView::OnDrawpic)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
END_MESSAGE_MAP()

// CTestView 构造/析构

CTestView::CTestView()
{
	// TODO: 在此处添加构造代码

}

CTestView::~CTestView()
{
}

BOOL CTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CView::PreCreateWindow(cs);
}

// CTestView 绘制

void CTestView::OnDraw(CDC* /*pDC*/)
{
	CTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 在此处为本机数据添加绘制代码
	DrawGraph();
}


// CTestView 打印

BOOL CTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加额外的打印前进行的初始化过程
}

void CTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加打印后进行的清理过程
}


// CTestView 诊断

#ifdef _DEBUG
void CTestView::AssertValid() const
{
	CView::AssertValid();
}

void CTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestDoc* CTestView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestDoc)));
	return (CTestDoc*)m_pDocument;
}
#endif //_DEBUG


// CTestView 消息处理程序


void CTestView::OnDrawpic() 
{
	// TODO: Add your command handler code here
	CColorDialog ccd(SeedClr,CC_SOLIDCOLOR);
	if(IDOK==ccd.DoModal())//调用颜色对话框选取填充色
		SeedClr=ccd.GetColor();	
	else
		return;	
	if(IDOK==MessageBox(CString("请在区域内部选取种子点"),CString("提示"),MB_OKCANCEL))
	    bFill=TRUE;
	else
		return;	
}
void CTestView::Push(CP2 point)//入栈函数
{
	pTop=new CStackNode;
	pTop->PixelPoint=point;
	pTop->pNext=pHead->pNext;
	pHead->pNext=pTop;
}

void CTestView::Pop(CP2 &point)//出栈函数
{
	if(pHead->pNext!=NULL)
	{
		pTop=pHead->pNext;
		pHead->pNext=pTop->pNext;
		point=pTop->PixelPoint;
		delete pTop;
	}
}

void CTestView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	Seed=CP2(point.x-rect.Width()/2,rect.Height()/2-point.y);//选择种子位置
	Invalidate(FALSE);
	CView::OnLButtonDown(nFlags, point);
}


void CTestView::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CView::OnLButtonUp(nFlags, point);
}

void CTestView::DrawGraph()//绘制图形
{
	CDC *pDC=GetDC();//定义pDC指针
	GetClientRect(&rect);
	pDC->SetMapMode(MM_ANISOTROPIC);//显示DC自定义坐标系
	pDC->SetWindowExt(rect.Width(),rect.Height());
	pDC->SetViewportExt(rect.Width(),-rect.Height());
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);
	CDC memDC;//声明一个内存DC
	memDC.CreateCompatibleDC(pDC);//创建一个与显示DC兼容的内存DC
	CBitmap NewBitmap,*pOldBitmap; 
	NewBitmap.LoadBitmap(IDB_BITMAP2);//从资源中导入空心汉字的位图
	BITMAP bmpInfo; //声明bmpInfo结构体
	NewBitmap.GetBitmap(&bmpInfo);//获取位图信息
	pOldBitmap=memDC.SelectObject(&NewBitmap);//将位图选入内存DC
	memDC.SetMapMode(MM_ANISOTROPIC);//内存DC自定义坐标系
	memDC.SetWindowExt(bmpInfo.bmWidth,bmpInfo.bmHeight);
	memDC.SetViewportExt(bmpInfo.bmWidth,-bmpInfo.bmHeight);
	memDC.SetViewportOrg(bmpInfo.bmWidth/2,bmpInfo.bmHeight/2);
	rect.OffsetRect(-rect.Width()/2,-rect.Height()/2);
	int nX=rect.left+(rect.Width()-bmpInfo.bmWidth)/2;//计算位图在客户区的中心点
    int nY=rect.top+(rect.Height()-bmpInfo.bmHeight)/2;
	pDC->BitBlt(nX,nY,rect.Width(),rect.Height(),&memDC,-bmpInfo.bmWidth/2,-bmpInfo.bmHeight/2,SRCCOPY);//将内存DC中的位图拷贝到设备DC
	if(bFill)
		CharFill(pDC);//填充空心汉字
	memDC.SelectObject(pOldBitmap);//从内存DC中释放位图
	memDC.DeleteDC();//删除memDC
	ReleaseDC(pDC);//释放pDC
}

void CTestView::CharFill(CDC *pDC)//文字填充函数
{
	COLORREF BoundaryClr=RGB(0,0,0);//边界色
	BOOL bSpanFill;
	pHead=new CStackNode;//建立栈结点
	pHead->pNext=NULL;//栈头结点的指针域总为空
	Push(Seed);//种子像素入栈
	int x,y,x0=Round(Seed.x),y0=Round(Seed.y);//x，y用于判断种子与图形的位置关系
	x=x0-1;
	while(pDC->GetPixel(x,y0)!=BoundaryClr && pDC->GetPixel(x,y0)!=SeedClr)//左方判断
	{
		x--;
		if(x<=-rect.Width()/2)
		{
			MessageBox(CString("种子不在图形之内"),CString("警告"));//到达客户区最左端
			return;
		}
	}
	y=y0+1;
	while(pDC->GetPixel(x0,y)!=BoundaryClr && pDC->GetPixel(x0,y)!=SeedClr)//上方判断
	{
		y++;
		if(y>=rect.Height()/2)//到达客户区最上端
		{
			MessageBox(CString("种子不在图形之内"),CString("警告"));//到达客户区最左端
			return;
		}
	}
	x=x0+1;
	while(pDC->GetPixel(x,y0)!=BoundaryClr && pDC->GetPixel(x,y0)!=SeedClr)//右方判断
	{
		x++;
		if(x>=rect.Width()/2)//到达客户区最右端
		{
			MessageBox(CString("种子不在图形之内"),CString("警告"));//到达客户区最左端
			return;
		}
	}
	y=y0-1;
	while(pDC->GetPixel(x0,y)!=BoundaryClr && pDC->GetPixel(x0,y)!=SeedClr)//下方判断
	{
		y--;
		if(y<=-rect.Height()/2)//到达客户区最下端
		{
			MessageBox(CString("种子不在图形之内"),CString("警告"));//到达客户区最左端
			return;
		}
	}
	double xleft,xright;//区间最左端与最右端像素
	CP2 PopPoint,PointTemp;
	while(pHead->pNext!=NULL)//如果栈不为空
	{
		Pop(PopPoint);
		if(pDC->GetPixel(Round(PopPoint.x),Round(PopPoint.y))==SeedClr)
			continue;
		PointTemp=PopPoint;
		while(pDC->GetPixel(Round(PointTemp.x),Round(PointTemp.y))!=BoundaryClr && pDC->GetPixel(Round(PointTemp.x),Round(PointTemp.y))!=SeedClr)
		{
			pDC->SetPixelV(Round(PointTemp.x),Round(PointTemp.y),SeedClr);
			PointTemp.x++;
		}
		xright=PointTemp.x-1;
		PointTemp.x=PopPoint.x-1;
		while(pDC->GetPixel(Round(PointTemp.x),Round(PointTemp.y))!=BoundaryClr && pDC->GetPixel(Round(PointTemp.x),Round(PointTemp.y))!=SeedClr)
		{
			pDC->SetPixelV(Round(PointTemp.x),Round(PointTemp.y),SeedClr);
			PointTemp.x--;
		}
		xleft=PointTemp.x+1;
		//处理上一条扫描线
		PointTemp.x=xleft;
		PointTemp.y=PointTemp.y+1;
		while(PointTemp.x<xright)
		{
			bSpanFill=FALSE;
			while(pDC->GetPixel(Round(PointTemp.x),Round(PointTemp.y))!=BoundaryClr && pDC->GetPixel(Round(PointTemp.x),Round(PointTemp.y))!=SeedClr)
			{
				bSpanFill=TRUE;
				PointTemp.x++;
			}
			if(bSpanFill)
			{
				if(PointTemp.x==xright && pDC->GetPixel(Round(PointTemp.x),Round(PointTemp.y))!=BoundaryClr && pDC->GetPixel(Round(PointTemp.x),Round(PointTemp.y))!=SeedClr)
					PopPoint=PointTemp;
				else
				{
					PopPoint.x=PointTemp.x-1;
					PopPoint.y=PointTemp.y;
				}
				Push(PopPoint);
				bSpanFill=FALSE;
			}
			while((pDC->GetPixel(Round(PointTemp.x),Round(PointTemp.y))==BoundaryClr && PointTemp.x<xright) || (pDC->GetPixel(Round(PointTemp.x),Round(PointTemp.y))==SeedClr && PointTemp.x<xright))
			    PointTemp.x++;
		}
		//处理下一条扫描线
		PointTemp.x=xleft;
		PointTemp.y=PointTemp.y-2;
		while(PointTemp.x<xright)
		{
			bSpanFill=FALSE;
			while(pDC->GetPixel(Round(PointTemp.x),Round(PointTemp.y))!=BoundaryClr && pDC->GetPixel(Round(PointTemp.x),Round(PointTemp.y))!=SeedClr)
			{
				bSpanFill=TRUE;
				PointTemp.x++;
			}
			if(bSpanFill)
			{
				if(PointTemp.x==xright && pDC->GetPixel(Round(PointTemp.x),Round(PointTemp.y))!=BoundaryClr && pDC->GetPixel(Round(PointTemp.x),Round(PointTemp.y))!=SeedClr)
					PopPoint=PointTemp;
				else
				{
					PopPoint.x=PointTemp.x-1;
					PopPoint.y=PointTemp.y;
				}
				Push(PopPoint);
				bSpanFill=FALSE;
			}
			while((pDC->GetPixel(Round(PointTemp.x),Round(PointTemp.y))==BoundaryClr && PointTemp.x<xright) || (pDC->GetPixel(Round(PointTemp.x),Round(PointTemp.y))==SeedClr && PointTemp.x<xright))
			    PointTemp.x++;
		}
	}
	delete pHead;
	pHead=NULL;
}
void CTestView::OnInitialUpdate()
{
	CView::OnInitialUpdate();
	// TODO: 在此添加专用代码和/或调用基类

}
